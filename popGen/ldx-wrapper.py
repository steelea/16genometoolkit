#!/usr/bin/env python

#   This is a multi-threaded script for running LDx.pl on a vcf file 
#   in a distributed fashion.

#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import os
import sys
import subprocess
import multiprocessing
import getopt

class LDX_Wrapper:
	def __init__(self, argv):
		self.argv = argv
		self.bam = ""
		self.vcf = ""
		self.reference = ""
		self.threads = 1
		self.scaffolds = []
		self.samtools = "samtools"
		self.ldx = "LDX.pl"
		self.ldxOptions = ""

	def usage(self):
		print("\nUsage: ldx-wrapper.py [OPTIONS]")
		print("OPTIONS:")
		print("  -b\tPool BAM file (Required)")
		print("  -v\tPool VCF file (Required)")
		print("  -r\tReference Fasta file (Required)")
		print("  -S\tPath to samtools (Required)")
		print("  -L\tPath to LDX.pl (Required)")
		print("  -t\tNumber of threads (default=1)\n")
		print("  -l\tMinimum read Depth>0 (default=10)")
		print("  -x\tMaximum read Depth>0 (default=100)")
		print("  -s\tPaired read Insert Size (default=500)")
		print("  -q\tPHRED quality score cutoff (default=20)")
		print("  -a\tAllele frequency cutoff (default=.1)")
		print("  -i\tMinimum intersection depth>0 (default=11)\n")
		print("  -h\tPrints this help message\n")

	def checkRequirements(self):
		#heck to make sure required files are accessible
		if not os.path.isfile(self.ldx):
			print("\n[ERROR] Unable to locate LDx.pl at given path")
			self.usage()
			sys.exit(1)

		if not os.path.isfile(self.samtools):
			print("\n[ERROR] Unable to locate samtools at the given path")
			self.usage()
			sys.exit(1)

		if not os.path.isfile(self.bam):
			print("\n[ERROR] Unable to access the BAM file at the given path")
			self.usage()
			sys.exit(1)

		if not os.path.isfile(self.vcf):
			print("\n[ERROR] Unable to access the VCF file at the given path")
			self.usage()
			sys.exit(1)

		if not os.path.isfile(self.reference):
			print("\n[ERROR] Unable to access the Fasta file at the given path")
			self.usage()
			sys.exit(1)
			
	def processArguments(self):
		try:
			opts,args = getopt.getopt(self.argv, "hb:v:r:t:l:x:s:q:a:i:", ["help","bam","vcf","ref_dict","threads"])
		except getopt.GetoptError:
			print "Unrecognized option"
			self.usage()
			sys.exit(2)		

		for opt, arg in opts:
			if opt in ("-h","--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-b","--bam"):
				self.bam = arg
			elif opt in ("-v","--vcf"):
				self.vcf = arg
			elif opt in ("-r","--reference"):
				self.reference = arg
			elif opt in ("-t", "--threads"):
				self.threads = int(arg)
			elif opt in ("-x"):
				self.ldx_options += " -h " + arg
			else:
				self.ldx_options += " " + opt + " " + arg				
				
	#Extract all scaffolds/chromosomes from dict file
	def parseFastaFile(self, filename):
		dict_file = open(filename,"r")
		lines = dict_file.readlines()
		dict_file.close()
		scaffolds = []
		for line in lines:
			if line.startswith("@SQ"):
				lsplit = line.split("\t")
				ssplit = lsplit[1].split(":")
				scaffolds.append(ssplit[1])
		return scaffolds

	def main(self):
		self.processArguments()
		self.checkRequirements()
		self.scaffolds = self.parse_dict_file(self.dict_file)

		for i in xrange(len(self.scaffolds)):
			self.scaffolds[i] += ":" + self.bam_file + ":" + self.vcf_file + ":" + self.ldx_options

		pool = multiprocessing.Pool(processes=self.threads)
		r = pool.map_async(ldx_scaffold, self.scaffolds)
		r.wait()


#Extract sorted sam for scaffold
def extract_scaf_sam(bam_filename,scaffold):
	sys.stderr.write("\tExtracting sam ....\n")
	sam_filename =  "." + scaffold + ".sam"
	command = "samtools view -h " + bam_filename + " " + scaffold + " > " + sam_filename
	proc = subprocess.Popen(command,shell=True)
	proc.wait()
	return sam_filename


#Extract vcf entries for scaffold
def extract_scaf_vcf(vcf_filename, scaffold):
	sys.stderr.write("\tExtracting vcf ....\n")
	vcf_scaf_filename = "." + scaffold + ".vcf"
	command = "grep \"" + scaffold + "\" " + vcf_filename + " > " + vcf_scaf_filename
	proc = subprocess.Popen(command,shell=True)
	proc.wait()
	return vcf_scaf_filename


#LDx a single scaffold/chromosome
def ldx_scaffold(args):
	#Hacked input passing
	inputs = args.split(":")
	scaffold = inputs[0]
	bam_file = inputs[1]
	vcf_file = inputs[2]
	options = inputs[3]	
	sys.stderr.write("Processing scaffold " + scaffold + "\n")
	
	#Pull out the scaffold sam/vcf
	sam_filename = extract_scaf_sam(bam_file, scaffold)
	vcf_filename = extract_scaf_vcf(vcf_file, scaffold)

	#Run LDx for the scaffold
	sys.stderr.write("\tRunning LDx....\n")
	command = "perl LDx.pl "+ options + " "  + sam_filename + " " + vcf_filename + " > " + scaffold + ".ldx.out"
	proc = subprocess.Popen(command,shell=True)
	proc.wait()

	#Remove the temporary sam/vcf file
	command = "rm " + sam_filename + " " + vcf_filename
	proc = subprocess.Popen(command, shell=True)
	proc.wait()

if __name__ == "__main__":
	lw = LDX_Wrapper(sys.argv[1:])
	lw.main()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

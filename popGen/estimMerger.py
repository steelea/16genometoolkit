#!/usr/bin/env python

#   This is a multi-threaded program to merge multiple .estim output 
#   files from poolHMM.

#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import sys
import multiprocessing
import subprocess
import os
import getopt
class estimMerger:
	def __init__(self,argv):
		self.argv = argv;
		self.threads = 1
		self.prefix = "out"
		self.fasta = ""
		self.estims = list()

	def usage(self):
		print("\nUsage: estimMerger.py [OPTIONS]")
		print("OPTIONS:")
		print("  -f\tFASTA file of reference assembly (Required)")
		print("  -e\tPool HMM .estim file (Required). Use once per .estim file")
		print("  -t\tNumber of threads to use (default=1)")
		print("  -p\tPrefix of output files (default=out)")
		print("  -h\tPrints this help message\n")

	def processArguments(self):
		if len(self.argv) < 1:
			self.usage()
			sys.exit(0)

		#Parse the arguments
		try:
			opts,args = getopt.getopt(self.argv,"hf:e:t:p:",["help","fasta","estim","threads","prefix"])
		except getopt.GetoptError:
			print("Unrecognized option")
			self.usage()
			sys.exit(1);

		for opt,arg in opts:
			if opt in ("-h", "--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-f","--fasta"):
				self.fasta = arg
			elif opt in ("-e","--estim"):
				self.estims.append(arg)
			elif opt in ("-t","--threads"):
				self.threads = int(arg)
			elif opt in ("-p","--prefix"):
				self.prefix = arg
			else:
				assert False, "Unhandled option"

		#Check required arguments
		if self.fasta == "":
			print("\n[ERROR] Missing input fasta file")
			self.usage()
			sys.exit(1)

		if len(self.estims) < 2:
			print("\n[ERROR] Insufficient .estim files for merging")
			print("\tMust have at least 2 .estim files")
			self.usage()
			sys.exit(1)

	#Parse the dictionary file
	def parseFastaSizes(self):
		print("Parsing the Fasta file:" + self.fasta)

		infile = open(self.fasta,"r")
		scaffolds = dict()

		header = ""
		size = 0
		line = infile.readline()
		while line:
			line = line.rstrip()
			if line.startswith(">"):
				if header != "":
					scaffolds[header] = size
				header = line[1:].split()[0]
				size = 0
			else:
				size += len(line)	
			line = infile.readline()
		scaffolds[header] = size
		infile.close()

		return scaffolds

	def main(self):
		#Get the scaffold information
		scaffolds = parseFastaSizes()
		
		#Create a pool of workers
		pool = multiprocessing.Pool(processes=self.threads)

		#Split each input file into subfiles for each scaffold  
		r=pool.map_async(splitEstimFile,[(e,i) for i,e in enumerate(self.estims)])
		r.wait()


		#Process single scaffold
		scafArgs = [(k,scaffolds[k],self.estims) for k in scaffolds.keys()]
		r=pool.map_async(processSingleScaf,scafArgs)
		r.wait()

		cat_command = "cat "
		rm_command = "rm "
		for s in scaffolds:
			cat_command = cat_command + s + ".tmp "
			rm_command = rm_command + s + ".tmp "
		cat_command = cat_command + "> " + merged_file

		print "Merging Temporary Files"
		proc = subprocess.Popen(cat_command,shell=True);
		proc.wait()

		print "Cleaning up"
		proc = subprocess.Popen(rm_command,shell=True);
		proc.wait()


def splitEstimFile(args):
	"Splitting Input:  " + args[0] 
	e = open(args[0],"r")
	i = args[1]
	o = None
	previous_scaf = "?"
	line = e.readline()
	while line:
		if previous_scaf == "?":
			lsplit = line.split()
			previous_scaf = lsplit[0]
			o = open(lsplit[0] + "." + str(i) + ".tmp","w")
			o.write(line)
		elif line.startswith(previous_scaf):
			o.write(line)
		else:
			o.close()
			lsplit = line.split()
			previous_scaf = lsplit[0]
			o = open(lsplit[0] + "." + str(i) + ".tmp","w")
			o.write(line)
		line = e.readline()
	e.close()


def processSingleScaf(args):
	scaf = args[0]
	size = args[1]
	estimFiles = args[2]
	fileCount = len(estimFiles)
	
	colOffset = 3
	refColumn = 2
	extraColumns = 1
	
	totalColumns = 2*fileCount + colOffset + extraColumns
	mergeInfo = [["" for j in range(totalColumns)] for i in range(size)]
 
	print "Processing scaffold: " + scaf		
	#Set all alternate to N and count to na
	for i in range(size):
		for j in range(totalColumns):
			if j == 0:
				mergeInfo[i][j] = scaf
			elif j == 1:
				mergeInfo[i][j] = i+1
			elif j == 2:
				mergeInfo[i][j] = "N"
			elif j == totalColumns-1:
				mergeInfo[i][j] = 0				
			elif j % 2 == 1:
				mergeInfo[i][j] = "N"
			else:
				mergeInfo[i][j] = "na"
		
	#Read through each of the scaffold files
	for i in range(file_count):
		if os.path.exists(scaf + "." + str(i) + ".tmp"):
			infile = open(scaf + "." + str(i) + ".tmp","r")
			lines = infile.readlines()
			for line in lines:
				lsplit = line.split()
				if len(lsplit) > 1:
					row = int(lsplit[1])-1
					alleleColumn = (i*2)+colOffset
					freqColumn = (i*2)+1+colOffset
				
					mergeInfo[row][alleleColumn] = lsplit[3]
					mergeInfo[row][freqColumn] = lsplit[4]
				
					if lsplit[refColumn].rstrip() in ('A','C','G','T'):
						mergeInfo[row][refColumn] = lsplit[refColumn]
									
			#Close and remove the tmp scaf file
			infile.close()
			command = "rm " + scaf + "." + str(i) + ".tmp"
			proc = subprocess.Popen(command,shell=True);
			proc.wait()
				
	#Set the last column for different alternate alleles
	for i in range(size):
		found = set()
		j = col_offset
		while j < totalColumns-1:
			found.add(mergeInfo[i][j])
			j+=2
		if "N" in found:
			found.remove("N")
		mergeInfo[i][totalColumns-1] = len(found)

	#Output all of the merge info to the file
	printstring = ""
	for i in range(size):
		printstring += '\t'.join(map(str,mergeInfo[i])) + '\n'

	#print printstring
	outfile = open(scaf + ".tmp","w")
	outfile.write(printstring)
	outfile.close()
		
	return 0
	
if __name__ == "__main__":
	em = estimMerger(sys.argv[1:])
	em.processArguments()
	em.main()
		

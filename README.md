# 16GenomeToolkit #

This is a collection of tools and scripts that were developed to aid with the Anopheline 16 Genome consortium.  The tools are developed to facilitate new methods of analysis as well as to simplify the use of existing tools. These tools are openly available for your use and redistribution under the GPL License.

### Getting Started ###

To get started with the tool you can acquire the executables from the [Downloads](https://bitbucket.org/steelea/16genometoolkit/downloads) page.  The source code is also available for download with git.


```bash 
git clone  https://bitbucket.org/steelea/16genometoolkit.git
```

Detailed information on what each tool does and how it can be used can be found on the [Wiki](https://bitbucket.org/steelea/16genometoolkit/wiki/Home) page.  Additionally, each tool will display detailed usage information if you run it without any input parameters or using the -h flag.


### Problems or Concerns ###

If you encounter and problems that appear to related to the tools (not your runtime environment) please submit and [Issues](https://bitbucket.org/steelea/16genometoolkit/issues) report and we will do our best to address your concerns in a timely manner.

### Cite Us ###

If you have made use of any of our tools or major portions of our variant calling pipeline please contact us on how to properly cite this toolkit.
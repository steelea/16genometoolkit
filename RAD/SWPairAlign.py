#!/usr/bin/env python

#   This is a program to idenitfy paired-end reads that have read through.
#   It creates 4 output files, _nrt files that don't have read through 
#   and _rt files where the forward and reverse reads overlap.

#   Copyright (C) <2014>  <Andres Martin>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import getopt
from itertools import izip

class SWPairAlign:
	def __init__(self,argv):
		self.argv = argv
		self.threshold = 20
		self.prefix= "out"
		self.fInputFile = ""
		self.rInputFile = ""

	def usage(self):
		print("\nUsage: SWPairAlign.py [OPTIONS]")
		print("OPTIONS:")
		print("  -f\tFastq file forward reads (Required)")
		print("  -r\tFastq file reverse reads (Required)")
		print("  -b\tThreshold of overlapping bases (default=20)")
		print("  -p\tPrefix of output files (default=out)")
		print("  -h\tPrints this help message\n")

	def processArguments(self):
		#Parse the arguments		
		try:
			opts, args = getopt.getopt(self.argv,"h:f:r:p:b:", ["help","forward","reverse","prefix","bases"])
		except getopt.GetoptError:
			print("Unrecognzied option")
			self.usage()
			sys.exit(1)
		
		for opt,arg in opts:
			if opt in ("-h","--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-f","--forward"):
				self.fInputFile = arg
			elif opt in ("-r","--reverse"):
				self.rInputFile = arg
			elif opt in ("-p","--prefix"):
				self.prefix = arg
			elif opt in ("-b","--bases"):
				self.threshold = int(arg)
			else:
				assert False, "Unhandled option"
				
		if self.fInputFile == "" or self.rInputFile == "":
			print("\n[ERROR] Missing input fastq files")
			self.usage()
			sys.exit(1)

	def main(self):
		#setting up input and output files
		t = self.threshold
		fMatch = self.prefix + "_1_rt.fq"
		rMatch = self.prefix + "_2_rt.fq"
		fNoMatch = self.prefix + "_1_nrt.fq"
		rNoMatch = self.prefix + "_2_nrt.fq"

		#opening and preping input
		fFile = open(self.fInputFile, 'r')
		rFile = open(self.rInputFile, 'r')

		#opening output
		fMatchFile = open(fMatch, 'w')
		rMatchFile = open(rMatch, 'w')
		fNoMatchFile = open(fNoMatch, 'w')
		rNoMatchFile = open(rNoMatch, 'w')

		#scoring system
		indelValue = -1
		matchValue = 1
		mismatchValue = 0
		baseComplement = {'A' : 'T', 'C' : 'G', 'G' : 'C', 'T' : 'A', 'N' : 'N'}
		lineCount = 0

		#loops through every read in the input
		for fLine, rLine in izip(fFile,rFile):
			if lineCount % 4 == 0:
				fSeq = fLine
				rSeq = rLine
	
			elif lineCount % 4 == 1:
				#reverse complements the current reads sequence
				rReverse = rLine[::-1].strip()
				rComplement = [baseComplement[base] for base in rReverse]
				fRead = list("^" + fLine)

				#sets up the boundrys on the reverse sequence. sets up matrix max
				maxT = len(rComplement) - t
				cT = 0
				maxValue = 0
				done = False

				#loops through all possible reverse sequences doing a local alignment every step of the way
				while(done == False):
					#sets up the matrix
					rRead =  list("^" + ''.join(rComplement[cT : cT + t]))
					matrix = []
					row = len(rRead) - 1
					col = len(fRead) - 1

					#puts 0's in the matrix
					for i in range(row+1):
						matrix.append([0]*(col+1))

					#fills in the matrix with the alignment
					for i in range(1, row+1):
						for j in range(1,col+1):
							left = matrix[i][j-1] + indelValue
							top = matrix[i-1][j] + indelValue
							if rRead[i] == fRead[j]:
								diag = matrix[i-1][j-1] + matchValue
							else:
								diag = matrix[i-1][j-1] + mismatchValue
							matrix[i][j] = max(left, top, diag, 0)

							#checks to see if there is a new max
							if matrix[i][j] > maxValue:
								maxValue = matrix[i][j]
						if maxValue < i - 1:
							break

					#if the max alignment is satifactory write the reads to the match files and move to the next read
					if maxValue >= t - 1:
						fMatchFile.write(fSeq)
						fMatchFile.write(fLine)
						rMatchFile.write(rSeq)
						rMatchFile.write(rLine)
						done = True
						match = True

					#if the last possible reverse sequence failed to satisfy then write the reads to the no match files
					elif cT == maxT:
						fNoMatchFile.write(fSeq)
						fNoMatchFile.write(fLine)
						rNoMatchFile.write(rSeq)
						rNoMatchFile.write(rLine)
						done = True
						match = False

					else:		
						cT += 1

			else:
				if match == True:
					fMatchFile.write(fLine)
					rMatchFile.write(rLine)

				else:
					fNoMatchFile.write(fLine)
					rNoMatchFile.write(rLine)

			lineCount += 1

if __name__ == "__main__":
	swpa = SWPairAlign(sys.argv[1:])
	swpa.processArguments()
	swpa.main()


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

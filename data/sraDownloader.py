#!/usr/bin/env python

#	This program downloads SRA data using accessiong numbers from a tab 
#	delimited .csv file

#	Copyright (C) <2014>  <Aaron Steele>
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import urllib2
import getopt

class SRADownloader:
	def __init__(self,argv):
		self.argv = argv
		self.csv = ""
		self.rootURL = "ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/"
		self.outDir = "."

	def usage(self):
		print("\nUsage: sraDownloader.py [OPTIONS]")
		print("OPTIONS:")
		print("  -c\tCSV file with sample and experiment accession numbers (Required)")
		print("  -o\tThe output directory where the files should be saved (default=.)")
		print("  -h\tPrints this help message\n")

	def processArguments(self):
		#Parse the arguments
		if len(sys.argv) < 2:
			self.usage()
			sys.exit(0)
		try:
			opts,args = getopt.getopt(self.argv,"hc:o:",["help","csv","out"])
		except getopt.GetoptError:
			print("Unrecongized option")
			self.usage()
			sys.exit(1)

		for opt, arg in opts:
			if opt in ("-h","--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-c","--csv"):
				self.csv = arg
			elif opt in ("-o","--out"):
				self.outDir = os.path.abspath(arg) 
			else:
				assert False, "Unhandled Option"

		#Check for required arguments
		if self.csv == "":
			print("\n[ERROR] Missing input CSV file")
			self.usage()
			sys.exit(1)

	def main(self):
		#Open the CSV and discard the header
		csvFile = open(self.csv,"r")
		csvFile.readline()

		#Column 1 = Sample Accession 
		#Column 2 = Experiment Accession
		
		line = csvFile.readline()
		while line:
			#Parse the accession information
			line = line.rstrip()
			lsplit = line.split("\t")
			sampAcc = lsplit[0]
			expAcc = lsplit[1]

			#Make the output directory
			if not os.path.isdir(self.outDir + "/" + sampAcc):
				os.makedirs(self.outDir + "/" + sampAcc)

			#Download the file
			print("Downloading " + sampAcc)
			baseURL = self.rootURL + expAcc[0:3] + "/" + expAcc[0:6] + "/" + expAcc
			ftpDirs = urllib2.urlopen(baseURL).read().splitlines()
			ftpFiles = [f.split()[-1] for f in ftpDirs]
			for f in ftpFiles:
				size = float(urllib2.urlopen(baseURL+"/"+ f).read().splitlines()[0].split()[4])/1000000
				print ("\t" + f + ".sra (" + "%.2f" + " MB)") % size
				outfile = open(self.outDir + "/" + sampAcc + "/"+ f + ".sra","wb")
				downURL = baseURL + "/" + f + "/" + f + ".sra"
				content = urllib2.urlopen(downURL)
				outfile.write(content.read())
				outfile.close()
						
			#Read the next line
			line = csvFile.readline()
		csvFile.close()
	
if __name__ == "__main__":
	sraD = SRADownloader(sys.argv[1:])
	sraD.processArguments()
	sraD.main()	 



# vim: tabstop=4 noexpandtab shiftwidth=4 softtabstop=4	

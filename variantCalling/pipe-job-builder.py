#!/usr/bin/env python

#   This is a program to Dynamically build sge jobs that will execute 
#   the ND varint discovery pipeline, based on BWA, Picard, and GATK.

#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import string
import math
import subprocess
import getopt

class Job_Builder:
	def __init__(self, argv):
		self.argv = argv
		self.validJobs = ["align","preprocess","bam-stats","ug","hc"]
		self.samples = []
		
		self.outFile = None
		self.memory = 2
		self.reference = ""
		self.job_name = "out"
		self.email = ""
		self.jobType = ""
		self.prefix = ""
		self.toolDir = ""

	def main(self):
		self.processArguments()
		#TODO Need an argument check here for robustness

		self.outFile = open(self.job_name,"w")
		self.buildJobHeader()
		self.buildJob()
		self.outFile.close()

	def usage(self):
		print("\nUsage: pipe-job-builder.py [OPTIONS]")
		print("  -i\tIndividual sample directory(Required or -d)")
		print("  -d\tDirecotry of sample directories(Required or -i)")
		print("  -r\tReference Assembly(Required)")
		print("  -j\tJob-Type[align,preprocess,bam-stats,ug,hc](Required)")
		print("  -e\tEmail address for SGE job")
		print("  -m\tAmount of memory in GB (default=2)")
		print("  -o\tOutput filename(default=out)")
		print("  -p\tPrefix for output files (default=\"\")")
		print("  -t\tDirectory of all the NGS tools (default=.)")
		print("  -h\tPrints this help message\n")	
 	
	def processArguments(self):
		try:
			opts, args = getopt.getopt(self.argv, "hm:d:i:j:o:r:e:p:t:", ["help","memory","dir","individual","job","output","reference","email", "prefix","tools"])

		except getopt.GetoptError:
			print "Unrecognized option"
			self.usage()
			sys.exit(2)

		for opt, arg in opts:
			if opt in ("-h", "--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-m","--memory"):
				self.memory = arg
			elif opt in ("-d","--dir"):
				if not arg.endswith("/"):
					arg = arg + "/"
				for subdirname in os.listdir(arg):
						if subdirname not in self.samples and os.path.isdir(arg+subdirname):
							self.samples.append(arg + subdirname + "/")			
			elif opt in ("-i","--individual"):
				if arg not in self.samples:
					if not arg.endswith("/"):
						arg = arg + "/"
					self.samples.append(arg)
			elif opt in ("-j","--job"):
				if arg not in self.valid_jobs:
					print "Unrecognized Job type"
					print "Valid jobs are: " + str(self.validJobs)
					sys.exit(1)
				else:
					self.jobType = arg
			elif opt in ("-o", "--output"):
				self.job_name = arg
			elif opt in ("-r", "--reference"):
				self.reference = arg
			elif opt in ("-e", "--email"):
				self.email = arg
			elif opt in ("-p", "--prefix"):
				self.prefix = arg + "_"
			elif opt in ("-t", "--tools"):
				self.toolDir = arg
				if not self.toolDir.endswith("/"):
					self.toolDir += "/"
			
			else:
				assert False, "Unhandled option"
						
	def buildJob(self):
		if self.jobType == "align":
			self.buildBWA()
		elif self.jobType == "preprocess":
			self.buildPreprocess()
		elif self.jobType == "bam-stats":
			self.buildBamStats()
		elif self.jobType == "ug":
			self.buildUG()
		elif self.jobType == "hc":
			self.buildHC()
		else:
			print("Unrecognized job type.  Please select a valid job\n")
			sys.exit(1)

	def buildJobHeader(self):
		self.outFile.write("#!/bin/csh\n")
		self.outFile.write("#$ -pe smp " + self.memory + "\n")
		if self.email != "":
			self.outFile.write("#$ -M " + self.email + "\n")
			self.outFile.write("#$ -m abe\n")


	#######################################
	####### BWA Standard ALN ##############
	#######################################
	def buildBWA(self):
		threads = self.memory		
		trimmed = False
		for s in self.samples:
	
			#Cheick if the samples are compressed and uncompress if necessary
			#Check to see if trimmed fq files are there if so use those...otherwise use regular
			trimmed = os.path.exists(s + "pe_1_trimmed.fq.gz") or os.path.exists(s + "pe_1_trimmed.fq")
			
			if not trimmed:
				if os.path.exists(s + "pe_1.fq.gz"):
					self.outFile.write("gunzip " + s + "pe_1.fq.gz\n")
				if os.path.exists(s + "pe_2.fq.gz"):
					self.outFile.write("gunzip " + s + "pe_2.fq.gz\n")

				#Write the alignment job
				self.outFile.write(self.toolDir + "bwa aln -k 2 -l 32 -o 1 -t " + str(threads) + " " + self.reference + " " + s + "pe_1.fq > " + s + self.prefix + "aln1.sai\n")
				self.outFile.write(self.toolDir + "bwa aln -k 2 -l 32 -o 1 -t " + str(threads) + " " + self.reference + " " + s + "pe_2.fq > " + s + self.prefix + "aln2.sai\n")
				self.outFile.write(self.toolDir + "bwa sampe " + self.reference + " " + s + self.prefix + "aln1.sai " + s + self.prefix + "aln2.sai " + s + "pe_1.fq " + s + "pe_2.fq > " + s + self.prefix + "aln_full.sam\n")
				self.outFile.write("rm " + s + self.prefix + "aln1.sai " + s + self.prefix + "aln2.sai\n")
	
				#Compress the pair-end files
				self.outFile.write("gzip " + s + "pe_1.fq\n")
				self.outFile.write("gzip " + s + "pe_2.fq\n")
			else:
				if os.path.exists(s + "pe_1_trimmed.fq.gz"):
					self.outFile.write("gunzip " + s + "pe_1_trimmed.fq.gz\n")
				if os.path.exists(s + "pe_2_trimmed.fq.gz"):
					self.outFile.write("gunzip " + s + "pe_2_trimmed.fq.gz\n")

				#Write the alignment job
				self.outFile.write(self.toolDir + "bwa aln -k 2 -l 32 -o 1 -t " + str(threads) + " " + self.reference + " " + s + "pe_1_trimmed.fq > " + s + self.prefix + "aln1.sai\n")
				self.outFile.write(self.toolDir + "bwa aln -k 2 -l 32 -o 1 -t " + str(threads) + " " + self.reference + " " + s + "pe_2_trimmed.fq > " + s + self.prefix + "aln2.sai\n")
				self.outFile.write(self.toolDir + "bwa sampe " + self.reference + " " + s + self.prefix + "aln1.sai " + s + self.prefix + "aln2.sai " + s + "pe_1_trimmed.fq " + s + "pe_2_trimmed.fq > " + s + self.prefix + "aln_full.sam\n")
				self.outFile.write("rm " + s + self.prefix + "aln1.sai " + s + self.prefix + "aln2.sai\n")

				#Compress the pair-end files
				self.outFile.write("gzip " + s + "pe_1_trimmed.fq\n")
				self.outFile.write("gzip " + s + "pe_2_trimmed.fq\n")
		
			#Clean and sort
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "picard/CleanSam.jar VALIDATION_STRINGENCY=LENIENT INPUT=" + s + self.prefix + "aln_full.sam O=" + s + self.prefix + "clean.sam\n")
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "picard/SortSam.jar SO=coordinate VALIDATION_STRINGENCY=LENIENT I=" + s + self.prefix + "clean.sam O=" + s + self.prefix + "aln_sorted.bam\n")

			#Free up some space
			self.outFile.write("rm " + s + self.prefix + "clean.sam " + s + self.prefix + "aln_full.sam\n\n")
           


	#######################################
	#### Picard + GATK Preprocessing ######
	#######################################

	def buildPreprocess(self):
		#self.outFile.write("module load R\n\n")
		self.outFile.write("module load java/1.7\n\n")
		threads = int(math.ceil(float(self.memory)/2))
		count = 0
		for s in self.samples:
			count = count + 1
			description = ""
			d = os.path.realpath(s)
			ssplit = d.split("/")
			sample_name = ssplit[-1]
			for i in ssplit:
				if i.startswith("16G_") or i == "ND6_pools": 
					description = i.replace("16G_","")
					break
				
			#Mark and Remove duplicates
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "picard/MarkDuplicates.jar VALIDATION_STRINGENCY=LENIENT MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 METRICS_FILE=" + s + self.prefix + "metrics.txt I=" + s + self.prefix + "aln_sorted.bam O=" + s + self.prefix + "aln_dedup.bam\n")

			#Add/Replace Read Groups
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "picard/AddOrReplaceReadGroups.jar INPUT=" + s + self.prefix + "aln_dedup.bam OUTPUT=" + s + self.prefix +"aln_rdgrp.bam SORT_ORDER=coordinate RGID=" + str(count) + " RGLB=solexa-123 RGPL=illumina RGPU=unknown RGCN=Broad RGDS=" + description + " RGSM=" + sample_name + "\n")

			#Index final picard Bam
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "picard/BuildBamIndex.jar VALIDATION_STRINGENCY=LENIENT INPUT=" + s + self.prefix + "aln_rdgrp.bam\n")

			#GATK indel realigner
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "GenomeAnalysisTK.jar -T RealignerTargetCreator -R " + self.reference + " -I " + s + self.prefix + "aln_rdgrp.bam -o " + s + self.prefix + "realigner.intervals -nt " + str(threads) + "\n")
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "GenomeAnalysisTK.jar -T IndelRealigner -R " + self.reference + " -I " + s + self.prefix + "aln_rdgrp.bam -targetIntervals " + s + self.prefix + "realigner.intervals -o " + s + self.prefix + "realigned.bam\n") 
			
			#Clean up mark duplicates and read group labeling
			self.outFile.write("rm " + s + self.prefix + "aln_dedup.bam " + s + self.prefix + "aln_rdgrp.bam\n\n")

		#Check to see if a reference dictionary exists, and create one if not
		dict_filename = ""

		if self.reference.endswith(".fa"):
			dict_filename = self.reference[:-3] + ".dict"
		elif self.reference.endswith(".fasta"):
			dict_filename = self.reference[:-6] + ".dict"
		else:
			dict_filename = self.reference + ".dict"

		isRefDict = False
		if os.path.exists(dict_filename):
			isRefDict = True
	
		if not isRefDict:
			self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "picard/CreateSequenceDictionary.jar REFERENCE=" + self.reference + " OUTPUT=" + dict_filename + "\n")
		
	
	##################################
	######## BAM STATS ###############
	##################################

	def buildBamStats(self):
		self.outFile.write("module load R\n\n")
		threads = int(math.ceil(float(self.memory)/2))

		for s in self.samples:
			#Set create the output directory
			out_dir = os.path.realpath(s)
			out_dir = out_dir.replace("samples","stats/bam-stats")
			
			if not out_dir.endswith("/"):
				out_dir = out_dir + "/"
	
			if not os.path.isdir(out_dir):
				os.mkdir(out_dir)

			#Remove any existing files
			if os.path.exists(out_dir + self.prefix + "aln_sorted.bamcheck"):
				self.outFile.write("rm " + out_dir + self.prefix + "aln_sorted.bamcheck\n")

			if os.path.exists(out_dir + self.prefix + "realigned.bamcheck"):
				self.outFile.write("rm " + out_dir + self.prefix + "realigned.bamcheck\n")

			#Bamcheck statistics
			self.outFile.write(self.toolDir + "bamcheck " + s + self.prefix + "aln_sorted.bam > " + out_dir + self.prefix + "aln_sorted.bamcheck\n") 
			self.outFile.write(self.toolDir + "plot-bamcheck -p " + out_dir + self.prefix + "aln_sorted " + out_dir + self.prefix + "aln_sorted.bamcheck\n") 
			self.outFile.write(self.toolDir + "bamcheck " + s + self.prefix + "realigned.bam > " + out_dir + self.prefix + "realigned.bamcheck\n")
			self.outFile.write(self.toolDir + "plot-bamcheck -p " + out_dir + self.prefix + "realigned " + out_dir + self.prefix + "realigned.bamcheck\n\n")

	################################
	### HAPLOTYPE CALLER ###########
	################################	
	def buildHC(self):
		#Determinethe appropriate number of threads
		threads = int(math.ceil(float(self.memory)/4))
		cpu_threads = 4
			
		#Write out all the standard parameters

		#Includ each sample bam
		for s in self.samples:
			self.outFile.write(" -I " + s + self.prefix + "realigned.bam")
		#Set the output file
		self.outFile.write(" -o variants/" + self.prefix + "hc-variants.vcf\n\n")
		
	##################################
	######## Unified Genotyper #######
	##################################
	def buildUG(self):
		self.outFile.write("module load java/1.7\n\n")

		#Determine the appropriate number threads
		threads = int(math.ceil(float(self.memory)/4))
		cpu_threads = 4
		
		#Write out all the standard parameters
		self.outFile.write("java -Xmx" + str(self.memory) + "g -jar " + self.toolDir + "GenomeAnalysisTK.jar -T UnifiedGenotyper -R " + self.reference + " --genotype_likelihoods_model BOTH --annotation AlleleBalance --computeSLOD  --output_mode EMIT_ALL_SITES -rf BadCigar --num_threads " + str(threads) + " --num_cpu_threads_per_data_thread " + str(cpu_threads))

		
		#Include each sample bam
		for s in self.samples:
			self.outFile.write(" -I " + s + self.prefix + "realigned.bam")

		#Set the output file
		self.outFile.write(" -o variants/" + self.prefix + "ug-variants.vcf\n\n")


if __name__ == "__main__":
	jb = Job_Builder(sys.argv[1:])
	jb.main()


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

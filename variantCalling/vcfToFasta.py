#!/usr/bin/env python

#   This is a program to convert a VCF file to a Fasta file selecting 
#   the majority allele at each site. This facilitiates additional 
#   population genomics based on WGA.
#
#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import getopt

class vcfToFasta:
	def __init__(self,argv):
		self.argv = argv
		self.vcf = ""
		self.prefix = "out"

	def usage(self):
		print("\nUsage: vcfToFasta.py [OPTIONS]")
		print("OPTIONS:")
		print("  -v\tVCF file to convert to Fasta (Required)")
		print("  -p\tPrefix of output Fasta file (default=out)")
		print("  -h\tPrints this help message\n")

	def processArguments(self):
		#Parse the arguments
		try:
			opts, args = getopt.getopt(self.argv, "hv:p:",["help","vcf","prefix"])
		except getopt.GetoptError:
			print("Unrecognized option")
			self.usage()
			sys.exit(1)

		for opt,arg in opts:
			if opt in ("-h","--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-v", "--vcf"):
				self.vcf = arg
			elif opt in ("-p", "--prefix"):
				self.prefix = arg
			else:
				assert False, "Unhandled Option"

		if self.vcf == "":
			print("\n[ERROR] Missing input vcf file")
			self.usage()
			sys.exit(1)
	
	def main(self):
		#------ Parse the VCF header -----#
		infile = open(self.vcf,"r")
		outfile = open(self.prefix + ".fasta")
		line = infile.readline()

		#Read Header garbage before contigs
		while not line.startswith("##contig"):
			line = infile.readline()

		#Get the contigs from the headers
		seqs = dict() 
		while line.startswith("##contig"):
			line = line[13:-2]
			lsplit = line.split(',')
			id = lsplit[0]
			length = lsplit[1].split('=')[1]
			seqs[id] = int(length)
			line = infile.readline()

		#Read any remaining header
		while line.startswith("#"):
			line = infile.readline()


		curScaf = ""
		header = ""
		seq = ""
		while line:
			lsplit = line.split()
	
			#Handle printing of scaffold sequences
			if curScaf == "":
				curScaf = lsplit[0]
				header = ">" + curScaf
				seq = ['N' for i in range(seqs[curScaf])]
			elif lsplit[0] != curScaf:
				outfile.write(header + "\n")
				outfile.wirte("".join(seq) + "\n")
				curScaf = lsplit[0]
				header = ">" + curScaf
				seq = ['N' for i in range(seqs[curScaf])]

			#NO SNP
			index = int(lsplit[1]) - 1
			if lsplit[4] == "." and len(lsplit[3]) == 1:
				seq[index] = lsplit[3]

			#Variant
			else:
				#Skip if deletion
				if len(lsplit[3]) > 1:
					sys.stderr.write("\n" + "Ignored deletion: " + lsplit[0] + "\t" + lsplit[1])
				#Skip if insertion or multiallelic
				elif len(lsplit[4]) > 1 and "," not in lsplit[4]:
					sys.stderr.write("\n" + "Ignored insertion: " + lsplit[0] + "\t" + lsplit[1])
				else:
					gsplit = lsplit[9].split(":")
					if gsplit[0] == "0/0":
						seq[index] = lsplit[3]
					elif gsplit[0] == "1/1":
						seq[index] = lsplit[4]
					else:
						alleles = list()
						alleles.append(lsplit[3])
						alleles = alleles + lsplit[4].split(",") 
						dsplit = gsplit[1].split(",")
			
						max = 0
						maxI = 0
						for i,value in enumerate(dsplit):
							if value > max:
								max = value
								maxI = i
						seq[index] = alleles[maxI]
			
			line = infile.readline()
		infile.close()

if __name__ == "__main__":
	vTF = vcfToFasta(sys.argv[1:])
	vTF.processArguments()
	vTf.main()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

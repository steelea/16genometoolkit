#!/usr/bin/env python

#   This is a program to add variant IDs to each site in a VCF file, 
#   which are necessary when filtering intervals with GATK.
#
#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import getopt

class vcfIdize:
	def __init__(self,argv):
		self.argv = argv
		self.vcf = ""
		self.prefix = "out"

	
	def usage(self):
		print("\nUsage: vcfIdize.py [OPTIONS]")
		print("OPTIONS:")
		print("  -v\tVCF file to add IDs to (Required)")
		print("  -p\tPrefix of the output VCF file (default=out)")
		print("  -h\tPrints this help message\n")

	
	def processArguments(self):
		#Parse the arguments
		try:
			opts, args = getopt.getopt(self.argv,"hv:p:",["help","vcf","prefix"])

		except getopt.GetoptError:
			print("Unrecongized option")
			self.usage()
			sys.exit(1)

		for opt, arg in opts:
			if opt in ("-h","--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-v", "--vcf"):
				self.vcf = arg
			elif opt in ("-p", "--prefix"):
				self.prefix = arg
			else:
				assert False, "Unhandled Option"

		#Check for required arguments
		if self.vcf == "":
			print("\n[ERROR] Missing input vcf file")
			self.usage()
			sys.exit(1)

	
	def main(self):
		infile = open(self.vcf,"r")
		outfile = open(self.prefix +"_ids.vcf","w")

		line = infile.readline()
		while line:
			if line.startswith("#"):
				outfile.write(line)
			else:
				lsplit = line.split("\t")
				lsplit[2] = lsplit[0] + ":" + lsplit[1]
				outfile.write("\t".join(map(str,lsplit)))
			line = infile.readline()
		infile.close()
		outfile.close()

if __name__ == "__main__":
	vi = vcfIdize(sys.argv[1:])
	vi.processArguments()
	vi.main()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4


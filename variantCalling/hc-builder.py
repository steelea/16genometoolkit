#!/usr/bin/env python

#   This is a program to dynamically build SGE jobs  for distributed 
#   execution of GATK's HaplotypeCaller.
#
#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import string
import math
import subprocess
import getopt

class Job_Builder:
	def __init__(self, argv):
		self.argv = argv
		self.gatk = ""
		self.prefix = "HC"
		self.memory = "2"
		self.bam = set()
		self.reference = ""
		self.email = ""
		self.vcf = "hc-varants.vcf"
		self.tasks = 4

	def usage(self):
		#TODO More detail here
		print("\nUsage: hc-builder.py [OPTIONS]")
		print("  -g\tPath to GenomeAnalysisTK.jar (Required)")
		print("  -b\tBAM file to call variants, use multiple times for multiple BAMs (Required)")
		print("  -r\tThe Reference fasta file (Required)")
		print("  -m\tAmount of memory/cores in GB per task (default=2)")
		print("  -t\tThe number of parallel tasks (default=4)")
		print("  -v\tThe name of the output VCF files (default=hc-variants.vcf)")
		print("  -p\tPrefix of the SGE job file (default=HC)")
		print("  -e\tEmail address for SGE status updates")
		print("  -h\tPrints this help message\n")
			
	def processArguments(self):
		#Parse the arguments
		try:
			opts, args = getopt.getopt(self.argv, "hg:b:r:m:t:v:p:e:", ["help","gatk","bam","reference","memory","tasks","vcf","prefix","email"])

		except getopt.GetoptError:
			print("Unrecognized option")
			self.usage()
			sys.exit(1)

		for opt, arg in opts:
			if opt in ("-h", "--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-g","--gatk"):
				self.gatk = arg
			elif opt in ("-b","--bam"):
				self.bam.add(arg)		
			elif opt in ("-r","--reference"):
				self.reference = arg
			elif opt in ("-m","--memory"):
				self.memory = arg
			elif opt in ("-r", "--reference"):
				self.reference = arg
			elif opt in ("-t", "--tasks"):
				self.tasks = int(arg)
			elif opt in ("-v", "--vcf"):
				self.vcf = arg
			elif opt in ("-p", "--prefix"):
				self.prefix = arg
			elif opt in ("-e", "--email"):
				self.email = arg
			else:
				assert False, "unhandled option"
				
		#Check for required arguments
		if self.gatk == "":
			print("\n[ERROR] Missing location of GATK")
			self.usage()
			sys.exit(1)
		if self.reference == "":
			print("\n[ERROR] Missing the fasta reference file")
			self.usage()
			sys.exit(1)
		if len(bams) == 0:
			print("\n[ERROR] Missing input BAM file(s)")
			self.usage()
			sys.exit(1) 
					
	def main(self):
		scaf_count = 0
		scaf_ids = []
		scaf_lengths = []
		big_scaf = 0;
		index = -1
		outfile = open(self.prefix + ".job","w")
	
		#Parse the reference file
		infile = open(self.reference,"r")
		line = infile.readline()
		while line:
			line = line.rstrip()
			if line.startswith(">"):
				line = line[1:]
				index += 1
				scaf_ids.append(line.split()[0])
				scaf_lengths.append(0)
				scaf_count += 1
			else:
				scaf_lengths[index] += len(line)

			line = infile.readline()
		infile.close()

		#Open the reference and count the scaffolds and get maximum
		big_scaf = max(scaf_lengths)		
	
		#Divide into bins that are smaller than the largest
		task_bins = []
		bin_sizes = []
		if self.tasks < 1:
			tmp_big = 0
			for i in range(scaf_count):
				tmp_big = max(scaf_lengths)
				index = scaf_lengths.index(tmp_big)
			
				inserted = False
				for i in range(len(task_bins)):
					if bin_sizes[i] + tmp_big <= big_scaf:
						task_bins[i] = task_bins[i] + " -L " + scaf_ids[index]
						bin_sizes[i] += tmp_big	
						inserted = True
				if not inserted:
					task_bins.append("-L " + scaf_ids[index])
					bin_sizes.append(tmp_big)
				
				#Remove the item
				del scaf_ids[index]
				del scaf_lengths[index]	
		
		else:
			for t in range(self.tasks):
				task_bins.append("")
				bin_sizes.append(0)
			for i in range(scaf_count):
				tmp_small = min(bin_sizes)
				index = bin_sizes.index(tmp_small)
				task_bins[index] = task_bins[index] + " -L " + scaf_ids[i]
				bin_sizes[index] += scaf_lengths[i]
		
		
		for t in range(len(bin_sizes)):
			sys.stderr.write("Task " + str(t+1) + ":" + str(bin_sizes[t]) + " bases\n")
		
		#Print the job header
		outfile.write("#!/bin/csh\n")
		if not self.email == "":
			outfile.write("#$ -M " + self.email + "\n")
			outfile.write("#$ -m abe\n")
		outfile.write("#$ -pe smp " + self.memory + "\n")
		
				
		#Write out task information
		outfile.write("#$ -t 1-" + str(len(task_bins)) + "\n\n")
		
		#Load required modules
		out_file.write("module load java/1.7\n\n")

		#Create the CSH task array
		out_file.write("set HC_TASKS = ( ")
		for t in task_bins:
			outfile.write("\"" + t + "\" ")
		outfile.write(")\n\n\n")

		#Write out all the standard parameters
		outfile.write("java -Xmx" + self.memory + "g -jar " + self.gatk + " -T HaplotypeCaller -R " + self.reference + " -rf BadCigar -nct" + self.memory)

		#Include each sample bam
		for b in self.bams:
			outfile.write(" -I " + b)

		#Set the variant file
		outfile.write(" -o " + self.vcf + ".$SGE_TASK_ID ")
		
		#Write out the intervals
		outfile.write("$HC_TASKS[$SGE_TASK_ID]")

		outfile.close()
if __name__ == "__main__":
	jb = Job_Builder(sys.argv[1:])
	jb.processArguments()
	jb.main()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

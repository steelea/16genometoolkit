#!/usr/bin/env python

#   This is a program to convert a fasta file to a VCF file without 
#   variants.  Useful for comparing to VCF files from other species.
#
#   Copyright (C) <2014>  <Aaron Steele>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import getopt

class fastaToVCF:
	def __init__(self,argv):
		self.argv = argv
		self.fasta = ""
		self.sample = ""
		self.prefix = "out"
		self.depth = "10"
		self.qual = "30"

	def usage(self):
		print("\nUsage: fastaToVCF.py [OPTIONS]")
		print("OPTIONS:")
		print("  -f\tFASTA file to convert to VCF (Required)")
		print("  -s\tSample name to be used in VCF (Required)")
		print("  -p\tPrefix of output files (default=out)")
		print("  -d\tDP for each all sites in VCF (default=10)")
		print("  -q\tQUAL for each site in VCF (default=30)")
		print("  -h\tPrints this help message\n")

	def processArguments(self):
		#Parse the arguments
		try:
			opts, args = getopt.getopt(self.argv, "hf:s:p:d:q:",["help","fasta","sample","prefix","depth","quality"])
		except getopt.GetoptError:
			print("Unrecognized option")
			self.usage()
			sys.exit(1)

		for opt,arg in opts:
			if opt in ("-h","--help"):
				self.usage()
				sys.exit(0)
			elif opt in ("-f","--fasta"):
				self.fasta = arg
			elif opt in ("-s","--sample"):
				self.sample = arg
			elif opt in ("-p","--prefix"):
				self.prefix = arg
			elif opt in ("-d","--depth"):
				self.depth = arg
			elif opt in ("-q","--qual"):
				self.qual = arg
			else:
				assert False, "Unhandled option"

		if self.fasta == "":
			print("\n[ERROR] Missing input fasta file")
			self.usage()
			sys.exit(1)

	def main(self):
		#Open the input/output files
		infile = open(self.fasta,"r")
		outfile = open(self.prefix + ".vcf","w")

		#Parse the Fasta file
		sys.stderr.write("Parsing the FASTA file\n")
		lines = infile.readlines()
		infile.close()
		headers = []
		seq = []
		tmp = ""
		for line in lines:
			line = line.rstrip()
			if line.startswith(">"):
				lsplit = line.split()
				headers.append(lsplit[0].replace(">",""))
				if tmp != "":
					seq.append(tmp)
					tmp = ""
			else:
				tmp += line.upper()
		seq.append(tmp)

		#Output the constantvcf header
		sys.stderr.write("Gerating the VCF header\n")
		outfile.write("##fileformat=VCFv4.1\n")
		outfile.write("##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Approximate read depth (reads with MQ=255 or with bad mates are filtered)\">\n")
		outfile.write("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n")

		#Output the contings to the headers
		for i in range(len(headers)):
			outfile.write("##contig=<ID=" + headers[i] + ",length=" + str(len(seq[i])) + ">\n")

		#Output the reference file in the header
		outfile.write("##reference=file://" + str(os.path.abspath(self.fasta)) + "\n")

		#Output the column labels
		outfile.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t" + self.sample + "\n")

		sys.stderr.write("Outputting VCF content\n")
		#Output the vcf information
		for i in range(len(headers)):
			for base in xrange(1,len(seq[i])+1):
				if seq[i][base-1] != 'N':
					outfile.write(headers[i] + "\t" + str(base) + "\t.\t" + str(seq[i][base-1]) + "\t.\t" + self.qual + "\t.\tDP=" + self.depth + "\tGT:DP\t0/0:" + self.depth + "\n")

		outfile.close()

if __name__ == "__main__":
	fTV = fastaToVCF(sys.argv[1:])
	fTV.processArguments()
	fTV.main()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
